Digio Teste

Bibliotecas Utilizadas:

* Dagger 2 : para injeção de dependência
* Retrofit 2 :  para comunicação com a API
* Roundedimageview : para arredondamento de bordas dos Imageviews
* Picasso : para carregamento de imagens nos ImageViews
* Corroutines : para operações em segundo plano
* Databinding

Para testes:

* Robolectric
* Expresso
* Mockito




