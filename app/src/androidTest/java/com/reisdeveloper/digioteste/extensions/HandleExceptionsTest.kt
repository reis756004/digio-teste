package com.reisdeveloper.digioteste.extensions

import android.accounts.NetworkErrorException
import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.reisdeveloper.digioteste.R
import com.reisdeveloper.digioteste.helper.simplifyExceptionsMessages
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class HandleExceptionsTest {

    lateinit var instrumentationContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun simplifyExceptionsMessages_ThrowableTypes() {
        val throws = listOf(
            SocketTimeoutException(),
            NetworkErrorException(),
            UnknownHostException(),
            Throwable()
        )

        throws.forEach {
            when(it){
                is SocketTimeoutException ->
                    assertThat(
                        instrumentationContext.getString(R.string.expired_time_try_again),
                        `is`(simplifyExceptionsMessages(instrumentationContext, it))
                    )

                is NetworkErrorException, is UnknownHostException ->
                    assertThat(
                        instrumentationContext.getString(R.string.no_server_connection_try_again),
                        `is`(simplifyExceptionsMessages(instrumentationContext, it))
                    )

                else ->
                    assertThat(
                        instrumentationContext.getString(R.string.something_went_wrong_try_again),
                        `is`(simplifyExceptionsMessages(instrumentationContext, it))
                    )
            }
        }
    }
}