package com.reisdeveloper.digioteste.ui

import android.app.Activity
import android.widget.ImageView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.reisdeveloper.digioteste.R
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SplashScreenActivityTest {

    @Test
    fun imageViewDisplayedTest() {
        val activityScenario = launchActivity<SplashScreenActivity>()
        onView(
            ViewMatchers.withContentDescription(
                activityScenario.getImageViewContentDescription()
            )
        ).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    private fun <T : Activity> ActivityScenario<T>.getImageViewContentDescription(): String {
        var description = ""
        onActivity {
            description =
                it.findViewById<ImageView>(R.id.splashscreen_imageView).contentDescription as String
        }
        return description
    }
}

