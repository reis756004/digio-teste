package com.reisdeveloper.digioteste.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.reisdeveloper.digioteste.getOrAwaitValue
import com.reisdeveloper.digioteste.models.Products
import io.reactivex.Maybe
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var mainViewModel: MainViewModel
    private lateinit var mainRepository: MainRepository

    @Before
    fun setup(){
        mainRepository = Mockito.mock(MainRepository::class.java)
        mainViewModel = MainViewModel(mainRepository)
    }

    @Test
    fun getProducts_Error(){
        val error = Throwable("Error")
        Mockito.`when`(mainRepository.getProductsRemoteDataSource()).thenReturn(Maybe.error( error ))

        mainViewModel.getProducts()

        assertEquals( error.message, mainViewModel._products.getOrAwaitValue().error?.message )
    }

    @Test
    fun testGetProducts_Success() {
        val obj = Products()
        Mockito.`when`(mainRepository.getProductsRemoteDataSource()).thenReturn(Maybe.just(obj))

        mainViewModel.getProducts()

        assertEquals( obj, mainViewModel._products.getOrAwaitValue().obj )
    }
}