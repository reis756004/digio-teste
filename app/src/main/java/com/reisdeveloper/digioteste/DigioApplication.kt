package com.reisdeveloper.digioteste

import android.app.Application
import com.reisdeveloper.digioteste.di.ApplicationComponent
import com.reisdeveloper.digioteste.di.DaggerApplicationComponent

open class DigioApplication : Application() {

    val appComponent: ApplicationComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): ApplicationComponent {
        return DaggerApplicationComponent.factory().create(applicationContext)
    }
}