package com.reisdeveloper.digioteste.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reisdeveloper.digioteste.api.ReturnLiveData
import com.reisdeveloper.digioteste.models.Products
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    private val _products = MutableLiveData<ReturnLiveData<Products>>()
    val products: LiveData<ReturnLiveData<Products>> = _products

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _error = MutableLiveData<Boolean>()
    val error: LiveData<Boolean> = _error

    fun getProducts(){
        _loading.postValue(true)
        _error.postValue(false)

        GlobalScope.launch( Dispatchers.IO ) {
            val response = ReturnLiveData<Products>()

            mainRepository.getProductsRemoteDataSource().subscribe(
                { returnObject ->
                    response.obj = returnObject
                    _products.postValue(response)
                    _loading.postValue(false)
                },
                { error ->
                    response.error = error
                    _products.postValue(response)
                    _error.postValue(true)
                }
            ).dispose()
        }
    }

    fun onRefrash(){
        getProducts()
    }

}

