package com.reisdeveloper.digioteste.ui.main

import com.reisdeveloper.digioteste.models.product.ProductsRemoteDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val productsRemoteDataSource: ProductsRemoteDataSource
) {

    fun getProductsRemoteDataSource() = productsRemoteDataSource.getRemoteProducts()

}