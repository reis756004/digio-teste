package com.reisdeveloper.digioteste.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.reisdeveloper.digioteste.DigioApplication
import com.reisdeveloper.digioteste.R
import com.reisdeveloper.digioteste.adapters.BannersAdapter
import com.reisdeveloper.digioteste.databinding.ActivityMainBinding
import com.reisdeveloper.digioteste.models.Banner
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    private var spotlightAdapter = BannersAdapter()
    private var productsAdapter = BannersAdapter()

    @Inject
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as DigioApplication).appComponent.inject(this)

        super.onCreate(savedInstanceState)

        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
                .apply {
                    lifecycleOwner = this@MainActivity
                    viewModel = mainViewModel
                }

        setBannerAdapter(binding.mainRvSpotlight, spotlightAdapter)

        setBannerAdapter(binding.mainRvProducts, productsAdapter)

        mainViewModel.products.observe(this, Observer { viewmodel ->
            if (viewmodel.error == null) {

                viewmodel.obj?.spotlight?.forEach {
                    addProductAdapter(spotlightAdapter, Banner().getBannerOfSpotlight(it))
                }

                viewmodel.obj?.products?.forEach {
                    addProductAdapter(productsAdapter, Banner().getBannerOfProduct(it))
                }
            }
        })

        getProducts()
    }

    private fun getProducts() {
        mainViewModel.getProducts()
    }

    private fun setBannerAdapter(recyclerView: RecyclerView, adapter: BannersAdapter) {
        with(recyclerView) {
            setHasFixedSize(true)
            itemAnimator = null
            this.adapter = adapter
            layoutManager = LinearLayoutManager(applicationContext, RecyclerView.HORIZONTAL, false)
        }
    }

    private fun addProductAdapter(adapter: BannersAdapter, banner: Banner) {
        adapter.addBanner(banner)
    }
}
