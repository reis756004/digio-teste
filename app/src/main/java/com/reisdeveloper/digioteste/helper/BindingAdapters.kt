package com.reisdeveloper.digioteste.helper

import android.accounts.NetworkErrorException
import android.content.Context
import android.view.View
import android.view.View.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.reisdeveloper.digioteste.R
import com.reisdeveloper.digioteste.enums.EnumTypeBanner
import com.squareup.picasso.Picasso
import java.net.SocketTimeoutException
import java.net.UnknownHostException

@BindingAdapter("imageUrl")
fun ImageView.loadImage(uri: String?) {
    Picasso.get()
        .load(uri)
        .error(R.drawable.ic_error_photo)
        .into(this)
}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean) {
    visibility = if (show) VISIBLE else GONE
}

@BindingAdapter("visible")
fun View.setVisible(show: Boolean) {
    visibility = if (show) VISIBLE else INVISIBLE
}

@BindingAdapter("bannerImageView")
fun View.bannerIsProductImageView(enumTypeBanner: EnumTypeBanner){
    if(enumTypeBanner == EnumTypeBanner.PRODUCTS){
        val params = LinearLayout.LayoutParams(300 , 300)
        this.layoutParams = params

        this.setPadding(60,60,60,60)
    }
}

@BindingAdapter("bannerLinearLayout")
fun View.bannerIsProductLinearLayout(enumTypeBanner: EnumTypeBanner){
    if(enumTypeBanner == EnumTypeBanner.PRODUCTS){
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(16,0,32,0)

        this.layoutParams = params
    }
}

@BindingAdapter(value = ["setAdapter"])
fun RecyclerView.bindRecyclerViewAdapter(adapter: RecyclerView.Adapter<*>) {
    this.run {
        this.setHasFixedSize(true)
        this.adapter = adapter
        layoutManager = LinearLayoutManager(this.context, RecyclerView.HORIZONTAL, false)
    }
}

@BindingAdapter("app:errorMessages")
fun simplifyExceptionsMessages(textView: TextView, error: Throwable?){
    if (error != null)
        textView.text = simplifyExceptionsMessages(textView.context, error)
    else
        textView.text = textView.context.getString(R.string.loading)
}

fun simplifyExceptionsMessages(context: Context, error: Throwable) : String {
    return when (error) {
        is SocketTimeoutException -> context.getString(R.string.expired_time_try_again)
        is NetworkErrorException, is UnknownHostException ->
            context.getString(R.string.no_server_connection_try_again)
        else -> context.getString(R.string.something_went_wrong_try_again)
    }
}



