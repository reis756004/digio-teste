package com.reisdeveloper.digioteste.enums

enum class EnumTypeBanner(var int: Int) {
    CASH(0),
    SPOTLIGHT(1),
    PRODUCTS(2);
}