package com.reisdeveloper.digioteste.models

class Cash(
    var title: String,
    var bannerURL: String,
    var description: String
)