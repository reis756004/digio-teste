package com.reisdeveloper.digioteste.models.product

import com.reisdeveloper.digioteste.api.DigioApi
import javax.inject.Inject

class ProductsRemoteDataSource @Inject constructor(
    private val digioApi: DigioApi
) {

    fun getRemoteProducts() = digioApi.getProducts()

}