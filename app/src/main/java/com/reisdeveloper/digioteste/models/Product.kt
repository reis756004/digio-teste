package com.reisdeveloper.digioteste.models

class Product(
    var name: String,
    var imageURL: String,
    var description: String
)