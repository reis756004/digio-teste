package com.reisdeveloper.digioteste.models

class Spotlight(
    var name: String,
    var bannerURL: String,
    var description: String
)