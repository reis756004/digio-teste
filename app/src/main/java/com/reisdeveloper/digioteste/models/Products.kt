package com.reisdeveloper.digioteste.models

class Products {
    var spotlight = mutableListOf<Spotlight>()
    var products = mutableListOf<Product>()
    var cash : Cash? = null
}