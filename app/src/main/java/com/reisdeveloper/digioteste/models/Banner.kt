package com.reisdeveloper.digioteste.models

import com.reisdeveloper.digioteste.enums.EnumTypeBanner

class Banner {
    var url: String = ""
    var description: String = ""
    var type: EnumTypeBanner = EnumTypeBanner.CASH

    fun getBannerOfSpotlight(spotlight: Spotlight): Banner{
        return Banner().apply {
            spotlight.bannerURL.let { url = it }
            spotlight.description.let { description = it }
            type = EnumTypeBanner.SPOTLIGHT
        }
    }

    fun getBannerOfProduct(product: Product): Banner{
        return Banner().apply {
            product.imageURL.let { url = it }
            product.description.let { description = it }
            type = EnumTypeBanner.PRODUCTS
        }
    }
}