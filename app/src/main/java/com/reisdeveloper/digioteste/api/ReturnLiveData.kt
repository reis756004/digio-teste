package com.reisdeveloper.digioteste.api

import androidx.lifecycle.MutableLiveData
import io.reactivex.Maybe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

open class ReturnLiveData<T> {
    var error: Throwable? = null
    var obj: T? = null

    companion object {
        fun <T> get(livedata: MutableLiveData<ReturnLiveData<T>>, callback: () -> Maybe<T>) =
            GlobalScope.launch( Dispatchers.IO ) {

                val response = ReturnLiveData<T>()

                callback().subscribe(
                    { returnObject ->
                        response.obj = returnObject
                        livedata.postValue(response)
                    },
                    { error ->
                        response.error = error
                        livedata.postValue(response)
                    }
                ).dispose()
            }
    }

}