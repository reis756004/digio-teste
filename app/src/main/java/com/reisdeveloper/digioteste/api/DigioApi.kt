package com.reisdeveloper.digioteste.api

import com.reisdeveloper.digioteste.models.Products
import io.reactivex.Maybe
import retrofit2.http.GET

interface DigioApi {
    @GET("sandbox/products")
    fun getProducts(): Maybe<Products>
}