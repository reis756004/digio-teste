package com.reisdeveloper.digioteste.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.reisdeveloper.digioteste.R
import com.reisdeveloper.digioteste.databinding.ListProductsBinding
import com.reisdeveloper.digioteste.models.Banner

class BannersAdapter(): RecyclerView.Adapter<BannersAdapter.BannersViewHolder>() {

    private var banners = mutableListOf<Banner>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannersViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_products, parent, false)
        return BannersViewHolder(view)
    }

    override fun getItemCount() = banners.size

    override fun onBindViewHolder(holder: BannersViewHolder, position: Int) {
        val binding = holder.binding
        binding?.banner = banners[position]
        binding?.executePendingBindings()
    }

    fun addBanner(banner: Banner){
        banners.add(banner)
        notifyDataSetChanged()
    }

    class BannersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ListProductsBinding? = DataBindingUtil.bind(itemView)
    }
}